# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

from ETL.wibe import wibe as scraper_wibe
from ETL.autocompara import autocompara as scraper_autocompra
from ETL.fiu import fiu as scraper_fiu
from ETL.ahorraseguro import ahorraseguro as scraper_ahorraseguro
from ETL.qualitas import qualitas as scraper_qualitas
from ETL.gnp import gnp as scraper_gnp
from ETL.interprotecciones import interprotecciones as scraper_interprotecciones
from ETL.mapfre import mapfre as scraper_mapfre
from ETL.banorte import banorte as scraper_banorte

class scrapers_execution():

    def __init__(self):
        self.scrapers_dict = {
            #'wibe':scraper_wibe,#LISTO
            'banorte' : scraper_banorte #LISTO
            #'autocompara':scraper_autocompra,#LISTO 
            #'fiu':scraper_fiu #LISTO
            #'interprotecciones':scraper_interprotecciones # LISTO
            #'ahorraseguro':scraper_ahorraseguro, #LISTO
            #'mapfre':scraper_mapfre,
            #'qualitas':scraper_qualitas,
            #'gnp':scraper_gnp,#PENDIENTE
            }
                              
    def start_extraction(self):
        for run_scraper in list(self.scrapers_dict.values()):
            instance_scraper = run_scraper.Scraper()
            flag_result = instance_scraper.run_spiders()
    
    def extract_one(self,player):
        run_scraper = self.scrappers_dict[player]
        print('Extrayendo ' + player)
        instance_scraper = run_scraper.Scraper()
        flag_result = instance_scraper.run_spiders()
        if flag_result == 'ok':
            return 1
        else:
            return 0

if __name__ == "__main__":
        
    main = scrapers_execution()

    run = main.start_extraction()

    

