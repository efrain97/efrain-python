"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

import requests as r
import configurations
from sqlalchemy import create_engine
import pandas as pd
from datetime import date
from urllib.parse import quote

class Scraper:
    
    def __init__(self):
        '''
        Constructor 
            inicializa los atributos de los objetos que se usaran en el script
        ''' 
        
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'es-MX,es;q=0.8,en-US;q=0.5,en;q=0.3',
            'Origin': 'https://cocheseguro.inter.mx',
            'Connection': 'keep-alive',
            'Referer': 'https://cocheseguro.inter.mx/inicio'
            }  

        self.name_catalogo = 'interprotecciones.csv'
        self.name_table = 'interprotecciones'

        self.df_catalog = pd.DataFrame()
        
        self.type = {
            'AUTO': 0,
            'PICKUP': 1,
            'MOTO': 9 
            }
        
        self.payload={}
        
        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database

    def get_items(self):
        '''
        Metodo get items
        Args:Objetos inicializados en el constructor 
        Desc: mediante peticiones compuestas se itera por tipos de vehiculos y por un rango de anios
        se encarga de extraer los vehiculos para asi ser apilados en un dataframe y al ser llenado 
        con todos los vehiculos del portal es guardado en la bd, en caso de alguna excepcion este dataframe 
        sera guardado en un excel de formato .CSV
        '''

        for t in self.type:

            for anio in range(2000, 2022):

                url = "https://lb3.copsis.com/data-transfer-api/MarcasListadoAPI?m=zl8mIfg3UrGeD1CvQu7UddMRtZ9eljnfGDTy+oOne/8=&s4_key=uLmdvb2dsZS5jb20vY2hhdC1kYXRhYmFzZS05NTdjMSIsIm5hbWUiOiJIZWJlc&" +\
                      "f1=" + str(self.type[t]) +"&f2=" + str(anio) + "&fc=29"
                brandsList = r.post(url=url, headers=self.headers, data=self.payload)

                try:
                    brands = brandsList.json()

                    for brand in brands:
                        print(brand['c2'])
                        urlSubrand = "https://lb3.copsis.com/data-transfer-api/TiposListadoAPI?m=zl8mIfg3UrGeD1CvQu7UddMRtZ9eljnfGDTy+oOne/8=&s4_key=uLmdvb2dsZS5jb20vY2hhdC1kYXRhYmFzZS05NTdjMSIsIm5hbWUiOiJIZWJlc&" +\
                                     "f1=" + str(self.type[t]) + "&f2=" + str(anio) + "&f3=" + brand['c1'] + "&fc=29"
                        subrandsList = r.get(url=urlSubrand, headers=self.headers, data=self.payload)
                        subrands = subrandsList.json()

                        for subrand in subrands:
                            urlVersion = "https://lb3.copsis.com/data-transfer-api/DescripcionListadoAPI?m=zl8mIfg3UrGeD1CvQu7UddMRtZ9eljnfGDTy+oOne/8=&s4_key=uLmdvb2dsZS5jb20vY2hhdC1kYXRhYmFzZS05NTdjMSIsIm5hbWUiOiJIZWJlc&" +\
                                         "f1=" + str(self.type[t]) + "&f2=" + str(anio) + "&f3=" + quote(brand['c1']) + "&f4=" + quote(subrand['c1']) + "&fc=29"
                            versionsList = r.get(url=urlVersion, headers=self.headers, data=self.payload)
                            versions = versionsList.json()

                            for version in versions:
                                values_catalog = {
                                    'marca': brand['c2'],
                                        'claveMarca': brand['c1'],
                                        'modelo': anio,
                                        'submarca': subrand['c1'],
                                        'version': version['c2'],
                                        'versionConIndex': "(" + str(version['c1']) + ")" + version['c2'],
                                        'indexPortal': version['c1'],
                                        'tipo': t,
                                        'claveTipo': self.type[t],
                                        'c7': '1',
                                        'c10': '0',
                                        'fecha_extraccion':date.today(),
                                        }

                                self.catalogo = self.catalogo.append(carro, ignore_index=True)
                                print('Insertado: ' + str(self.catalogo))

                except Exception as e:
                    print('Error marcas: ' + str(e))
        try:
            engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
        except Exception as a:
                print('Error en la conexion interprotecciones: ' + str(a))

        try:
            self.catalogo.to_sql(self.name_table, con=engine, if_exists='append', index=False)
        except Exception as a:
            print('Error al Guardar en la BD: ' +a)
            try:
                self.catalogo.to_csv(self.name_catalogo, sep='@', encoding='latin1')
            except Exception as a:
                print('Error al generar CSV: ' + a)
            pass
             
    def run_spiders(self):
        '''
        Metodo run spiders
        Args: recibe los objetos inicializados
        Desc: este metodo se encarga de ser llamado por la clase run_scrapers, y este mismo se encarga de ser 
        llamado en el mismo script para inciar el proceso
        '''
        registros = self.get_items()
        print('Total de registros extraidos INTER: ' + str(registros))
         
        
