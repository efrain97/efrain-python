# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

import requests
import pandas as pd
import configurations
from datetime import date
from sqlalchemy import create_engine

class Scraper:
    
    def __init__(self):
        '''
        Constructor 
            inicializa los atributos de los objetos que se usaran en el script
        ''' 

        self.df_catalog = pd.DataFrame()
        
        self.headers = {
            'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
            'Content-Type' : 'application/json',
            'Origin' : 'https://www.autocompara.com',
            'Cookie' : 'Forgotten; eea635e87ac58a3f6d8b6df7591ef0dc=e5861f67e7c82a8478b4e0950f6257fe'            
            }
        
        self.name_catalogo = 'catalogo_autocompara.csv'

        self.name_table = 'autocompara'
        
        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database
        
        self.types_car_value = {
            'auto' : 1,
            'pickup' : 2
            }

    def get_items(self):
        '''
        Metodo get items 
        Args : recibe los objetos iniciaizados 
        Desc : Se realizan peticiones para obtener los vehiculos mediante los tipos de vehiculos
        para ser guardados en un dataframe y al termino de este el mismo sea guardado en la base de datos. Dado el caso 
        de una excepcion sera guardado el catalogo en un Excel formato .csv
        '''
        
        for type_vehiculo in self.types_car_value.keys():

            id_tipo = str(self.types_car_value[type_vehiculo])
            url_anios = 'https://www.autocompara.com/autocompara/v1/catalogosfnx/vehiculos/modelos/' + str(id_tipo)
            coun_pet = 0
            
            while(coun_pet < 3):
                try:
                    anios = requests.request("GET", url=url_anios, headers=self.headers).json()
                    break
                except Exception:
                    print('error extrayendo los anios')
                    coun_pet += 1

            for an in anios:
                url_modelos = 'https://www.autocompara.com/autocompara/v1/catalogosfnx/vehiculos/' + id_tipo + '/' + str(an)
                cont = 0
                struct_autos = requests.request("GET", url=url_modelos, headers=self.headers)
                
                while(cont <= 15):
                    try:
                        struct_autos = struct_autos.json()
                    except Exception as a:
                        print('Error extrayendo los modelos: ' + str(a))

                    if(len(struct_autos) != 0):
                        break
                    cont += 1
                
                for i in range(len(struct_autos)):
                    count_car = 0

                    insumo_actual = struct_autos[i]
                    idCuac = insumo_actual['idCuac']
                    version = insumo_actual['version']
                    idMarca = insumo_actual['idMarca']
                    marca = insumo_actual['marca']

                    count_car += 1
   
                    values_catalog = {
                        'Tipo': type_vehiculo.upper(),
                        'id_tipo': id_tipo,
                        'Modelo': str(an),
                        'Marca': marca,
                        'id_marca':idMarca,
                        'version':version,
                        'id_Cuac':str(idCuac),
                        'fecha_extraccion':date.today(),
                        }

                    self.df_catalog = self.df_catalog.append(values_catalog, ignore_index=True)
                    print('Insertado: ' + str(self.df_catalog))

        try:
            engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))                  
        except Exception as a:
            print('Error en la conexion autocompara: ' + str(a))

        try:
            self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
        except Exception as a:
            try:
                self.df_catalog.to_csv(self.name_catalogo , sep = '@', encoding = 'latin1')
            except Exception as a:
                print('Error al guardad en BD: ' + a)
            pass

    def run_spiders(self):
        '''
        Metodo run spiders
        Args: recibe el objeto para hacer funcionar el script get items
        desc: es el metodo encargado de hacer funcionar el script, este mismo manda 
        a ser llamado desde la clase principal llamada run_scrapers
        '''
        obj = self.get_items()
        print('total de registros extraidos AUTOCOMPARA: ' + str(obj))
        print('finalizado-autocompara')

        
        
    