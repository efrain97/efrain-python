# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

import urllib3
import requests
import pandas as pd
import configurations
from datetime import date
from sqlalchemy import create_engine
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from bs4 import BeautifulSoup

class Scraper:

    def __init__(self):
        '''
        Constructor 
            inicializa los atributos de los objetos que se usaran en el script
        ''' 
        self.df_catalog = pd.DataFrame()

        self.url_marcas = 'https://cotizadorautos.mapfre.com.mx/rates/getbrands'
        self.url_submarcas = 'https://cotizadorautos.mapfre.com.mx/rates/getvehicle'
        self.url_anios = 'https://cotizadorautos.mapfre.com.mx/rates/getmodels/'

        self.name_catalogo = 'catalogo_mapfre.csv'

        self.name_table = 'mapfre'

        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database

    def get_items(self):
        '''
        Metodo: get_items
        Args: recibe los objetos inicializados
        Desc: este metodo se encarga de extraer los vehiclos en dos etapas primero por incidencias no encontradas
        en el portal para normalizar los datos ejemplo primera incial en minuscula al termino de esto guarda en el 
        dataframe oara ser guardado en la bd, despues los vehiculos que no hay que normalizar los que ya se encuentran,
        al termino de la extraccion los datos seran guardados en un dataframe para asi ser insertados en la BD, 
        cualquier excepcion sera guardado en un Excel de formato . CSV 
        '''

        requests.packages.urllib3.disable_warnings(InsecureRequestWarning) #metodo para no mostrar los warning
        s = requests.Session()
        contador = 0
        bandera = 1

        while(bandera == 1):
            try:
                submarcas_array = s.get(self.url_submarcas,verify=False).json()
                marcas_array = s.get(self.url_marcas, verify=False).json()
                bandera = 0
            except Exception:
                pass
        marcas = []
        for marc in marcas_array:
            for marcs in submarcas_array:
                if(marc['id'] == marcs['id_brand']):
                    marcs['marca'] = marc['name']
                    marcs['submarca'] = marcs['name'].replace(marcs['marca'] + ' ',"")


        for insum in submarcas_array:
            try:
                array_anios = s.get(self.url_anios + str(insum['id']), verify=False).json()
                anios = (x['year'] for x in array_anios)
                insum['anios'] = list(anios)
            except Exception:
                pass

        submarcas_aux = []

        for insum in submarcas_array:
            for anio in insum['anios']:
                mar = str(insum['marca'])
                submar = str(insum['submarca'])
                if(submar == 'Attitude'):
                    submar = 'atittude'
                elif(submar == 'Concorde'):
                    submar = 'Concord'
                elif(submar == 'LeBaron'):
                    submar = 'Le-Baron'
                elif(submar == 'IX35'):
                    submar = 'IX-35'
                elif(submar == '240SX'):
                    submar = '240-SX'
                elif(submar == '300ZX'):
                    submar = '300-ZX'
                elif(submar == '350Z'):
                    submar = '350-Z'
                elif(submar == '370Z'):
                    submar = '370-Z'
                elif(submar == 'XV'):
                    submar = 'vx'
                elif(submar == 'Touareg'):
                    submar = 'tourag'
                    
                url = 'https://cotizadorautos.mapfre.com.mx/rates/car/' + mar + '/' + submar + '/' + str(anio) + '/11000/1997-06-09/m'
                cont = 0
                while(cont <= 5):
                    try:
                        respuesta = s.get(url,verify=False, allow_redirects=False).status_code
                        if(respuesta == 200):
                            break
                        else:
                            cont += 1
                    except Exception:
                        pass
            
                if (respuesta == 200):
                    submarcas_aux.append(submar)
                    try:
                        raspado = s.get(url, verify=False, allow_redirects=False)
                        soup = BeautifulSoup(raspado.text, "lxml")
                    except Exception:
                        pass
                    
                    try:
                        versiones_soup = soup.find_all(id='version',attrs={"name" : "id_version"})
                        for versiones in versiones_soup:
                            for data in versiones.find_all('option'):
                                version = data.text
                                id_version = data.get('value')

                                engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
                                
                                try:
                                    self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
                                    contador += 1
                                except Exception as a:
                                    try:
                                        self.df_catalog.to_csv(self.name_catalogo , sep = '@', encoding = 'latin1')
                                    except Exception as a:
                                        print('Error al guardad en BD: ' + a)
                                    pass
                                

                    except Exception as e:
                        print(e)

                        
        for insum in submarcas_array:
            if(insum['submarca'] not in submarcas_aux):
                for anio in insum['anios']:
                    mar = str(insum['marca']).replace(" ", '-')
                    submar = str(insum['submarca']).replace('-','')
                    url = 'https://cotizadorautos.mapfre.com.mx/rates/car/' + mar + '/' + submar + '/' + str(anio) + '/11000/1997-06-09/m'
                    cont = 0
                    while(cont <= 5):
                        try:
                            respuesta = s.get(url, verify=False, allow_redirects=False).status_code
                            if(respuesta == 200):
                                break
                            else:
                                cont += 1
                        except Exception:
                            pass
                        
                    if (respuesta == 200):
                        submarcas_aux.append(submar)
                        try:
                            raspado = s.get(url, verify=False, allow_redirects=False)
                            soup = BeautifulSoup(raspado.text, "lxml")
                        except Exception:
                            pass
                        
                        try:
                            versiones_soup = soup.find_all(id='version',attrs={"name" : "id_version"})
                            for versiones in versiones_soup:
                                for data in versiones.find_all('option'):
                                    version = data.text
                                    id_version = data.get('value')
                                    engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                                        host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
                                
                                    try:
                                        self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
                                        contador += 1
                                    except Exception as a:
                                        try:
                                            self.df_catalog.to_csv(self.name_catalogo , sep = '@', encoding = 'latin1')
                                        except Exception as a:
                                            print('Error al guardad en BD: ' + a)
                                        pass

                        except Exception as e:
                            print(e)

        for insum in submarcas_array:
            if(insum['submarca'] not in submarcas_aux):
                for anio in insum['anios']:
                    mar = str(insum['marca']).replace(" ", '-')
                    submar = str(insum['submarca']).replace('-','').replace(' ','-')
                    url = 'https://cotizadorautos.mapfre.com.mx/rates/car/' + mar + '/' + submar + '/' + str(anio) + '/11000/1997-06-09/m'
                    cont = 0
                    while(cont <= 5):
                        try:
                            respuesta = s.get(url, verify=False, allow_redirects=False).status_code
                            if(respuesta == 200):
                                break
                            else:
                                cont += 1
                        except Exception:
                            pass
                            
                    if (respuesta == 200):
                        submarcas_aux.append(submar)
                        try:
                            raspado = s.get(url, verify=False, allow_redirects=False)
                            soup = BeautifulSoup(raspado.text, "lxml")
                        except Exception:
                            pass
                        
                        try:
                            versiones_soup = soup.find_all(id='version',attrs={"name" : "id_version"})
                            for versiones in versiones_soup:
                                for data in versiones.find_all('option'):
                                    version = data.text
                                    id_version = data.get('value')
                                    engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                                        host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
                                
                                    try:
                                        self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
                                        contador += 1
                                    except Exception as a:
                                        try:
                                            self.df_catalog.to_csv(self.name_catalogo , sep = '@', encoding = 'latin1')
                                        except Exception as a:
                                            print('Error al guardad en BD: ' + a)
                                        pass
                                        
                        except Exception as e:
                            print(e)

        s.close()

    def run_spiders(self):
        print('vehiculos insertados: ' + str(contador))
        print('terminado')