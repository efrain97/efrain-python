# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

import requests
import time
from bs4 import BeautifulSoup
from selenium import webdriver
import configurations
from sqlalchemy import create_engine
from datetime import date

class Scraper:
    
    def __init__(self):
        
        self.name_catalogo="catalogo_Qualitas.csv"
        
        self.limite_peticiones = 20
        
        self.url_modelos = 'https://agentes.qualitas.com.mx/agentes/cotizacion/GetValues.view?marcaAjax=true&validateFormAjax=true&nuevoFlujo=true&negocioTarifaServicioUso=1|1000|1|1'
        
        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database

    def obtener_insumos(self):
        contador = 0
        
        cont = 0
        #actualiza cookie
        browser = webdriver.Firefox()
        while(1):
            try:
                #browser.get('https://agentes.qualitas.com.mx/agentes/Menu.do?publico=true')
                browser.get('https://agentes.qualitas.com.mx/agentes/year.qualitas')
                
                break
            except Exception:
                time.sleep(0.5)
        coockie = 'COT_AGE_JSESSIONID=' + 'jxvllm7Bwa91P2_I5wSuOzODi80Q9xRfYgg34f-zEmAwoHVdJ_Cl!-260114351'
        #coockie = 'COT_AGE_JSESSIONID=' +'BinS_NM3MBcm1eegjh53ld5AevaLZjv7vhSBenPFIZf_EYQ_PUtu!-1487088753'
        encabezados = {
        'Cookie': coockie
        }
        browser.close()
        while(cont <= self.limite_peticiones):
            peticion_modelos = s.post(self.url_modelos, headers = encabezados)
            modelos = BeautifulSoup(peticion_modelos.text, "lxml").find_all('option')
            if (len(modelos)!= 0):
                break
            else:
                cont += 1
    
    
        anios = []
        for option in modelos:
            anios.append(option['value'])
    
        for an in anios:
            url_marcas = 'https://agentes.qualitas.com.mx/agentes/cotizacion/GetValues.view?marcaAjax=true&validateFormAjax=true&nuevoFlujo=true&modelo=' + an + '&negocioTarifaServicioUso=1|1000|1|1'
            cont = 0
            while(cont <= limite_peticiones):
                peticion_marcas = s.post(url_marcas,headers = encabezados)
                marcas_soup = BeautifulSoup(peticion_marcas.text, "lxml").find_all('option')
                if(len(marcas_soup) != 0):
                    break
                else:
                    cont += 1
    
            marcas_value = []
            marcas = []
            for option in marcas_soup:
                marcas_value.append(option['value'])
                marcas.append(str(option).split('>')[1].split('<')[0])
    
            dic_marcas = dict(zip(marcas_value,marcas))
    
            for marc in marcas_value:
                marca = dic_marcas[marc]
                url_modelos = 'https://agentes.qualitas.com.mx/agentes/cotizacion/GetValues.view?tipoAjax=true&validateFormAjax=true&negocioTarifaServicioUso=1|1000|1|1&nuevoFlujo=true&modelo=' + str(an) + '&marca=' + str(marc)
                cont = 0
                while(cont <= limite_peticiones):
                    peticion_modelos = s.post(url_modelos,headers = encabezados)
                    modelos_soup = BeautifulSoup(peticion_modelos.text, "lxml").find_all('option')
                    if (len(modelos_soup) != 0):
                        break
                    else:
                        cont += 1
    
                modelos_value = []
                modelos = []
                for option in modelos_soup:
                    modelos_value.append(option['value'])
                    modelos.append(str(option).split('>')[1].split('<')[0])
    
                dic_modelos = dict(zip(modelos_value,modelos))
    
                for mod in modelos_value:
                    modelo = dic_modelos[mod]
                    url_version = 'https://agentes.qualitas.com.mx/agentes/cotizacion/GetValues.view?tipoAjax=true&validateFormAjax=true&nuevoFlujo=true&negocioTarifaServicioUso=1|1000|1|1&modelo=' + str(an) + '&marca=' + str(marc) + '&tipo=' + str(mod)
                                  #https://agentes.qualitas.com.mx/agentes/cotizacion/GetValues.view?tipoAjax=true&validateFormAjax=true&nuevoFlujo=true&negocioTarifaServicioUso=1|1000|1|1&modelo=      2017     &marca=       BW        &tipo=     77
                    cont = 0
                    while (cont <= limite_peticiones):
                        peticion_version = s.post(url_version,headers = encabezados)
                        versiones_soup = BeautifulSoup(peticion_version.text, "lxml").find_all('option')
                        if(len(versiones_soup) != 0):
                            break
                        else:
                            cont += 1
    
                    versiones_value = []
                    versiones = []
                    for option in versiones_soup:
                        versiones_value.append(option['value'])
                        versiones.append(str(option).split('>')[1].split('<')[0])
    
                    dic_versiones = dict(zip(versiones_value,versiones))
    
                    for ver in versiones_value:
                        contador += 1
                        version = dic_versiones[ver]
                        try:
                            fileCSV = open(self.name_catalogo, "a")
                            fileCSV.write(str(marca) + '@' + str(marc) + '@' + str(an) + '@' + str(modelo) + '@' + str(mod) + '@' + str(version) + '@' + str(ver) + '\n')
                            fileCSV.close()
                        except Exception as e:
                            print(e)
        return (contador)
    
    def run_spiders(self):
    
        
        s = requests.Session()
        
        fileCSV = open(self.name_catalogo, 'w')
        fileCSV.write('marca@id_marca@anio@modelo@id_modelo@version@id_version'+'\n')
        fileCSV.close()
        cont = self.obtener_insumos()
        print('total de autos insertados: ' + str(cont))
        print('finalizado')