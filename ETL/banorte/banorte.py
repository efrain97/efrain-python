# -*- coding: utf-8 -*-
"""
Created on Wed Sep  29 09:30:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""
import requests
import configurations
import pandas as pd
import json
from datetime import date
from sqlalchemy import create_engine

class Scraper:

    def __init__(self):
        self.url_categories = "https://servicios.segurosbanorte.com/autosapi/categorias"
        self.url_brands_subbrands = 'https://www.segurosbanorte.com.mx/segurosbxi/marcas-submarcas'
        self.url_vehicles = 'https://www.segurosbanorte.com.mx/segurosbxi/vehiculos'

        self.headers = {
            'Connection': 'keep-alive',
            'sec-ch-ua': '"Google Chrome";v="93", " Not;A Brand";v="99", "Chromium";v="93"',
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'sec-ch-ua-mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36',
            'sec-ch-ua-platform': '"Windows"',
            'Origin': 'https://www.segurosbanorte.com.mx',
            'Sec-Fetch-Site': 'cross-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://www.segurosbanorte.com.mx/',
            'Accept-Language': 'es-419,es;q=0.9',
            'Cookie': 'BIGipServerP_DC213_9899_914A=1664329920.36899.0000'
            }

        self.name_catalogo = 'banorte_catalogo.csv'
        self.name_table = 'banorte'

        self.df_catalog = pd.DataFrame()

        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database

    def get_items(self):

        payload = json.dumps({"usuarioCliente":"13079","clienteEstrategico":"bxi","producto": "PORTAL DE INTERNET"})
        response = requests.request("POST", self.url_categories, headers=self.headers, data=payload)
        struct_categories = response.json()
        for struct_categorie in struct_categories['categorias']:
            id_cat = str(struct_categorie['id'])
            cat = struct_categorie['nombre']

            for anio in range(2003, 2022):
                payload = json.dumps({"usuarioCliente":"13079","clienteEstrategico":"bxi","producto":"PORTAL DE INTERNET","categoria":cat,"anioVehiculo":str(anio)})
                url_brands_subbrands = 'https://www.segurosbanorte.com.mx/segurosbxi/marcas-submarcas'
                response_subbrands = requests.request("POST", self.url_brands_subbrands, headers=self.headers, data=payload)
                
                try:
                    struct_subbrands = response_subbrands.json()
                    for struct_subbrand in struct_subbrands['data']['categorias']:
                        for subbrands in struct_subbrand['submarcas']:
                            id_subMarca = str(subbrands['id'])
                            subMarca = subbrands['nombre']
                            payload = json.dumps({"usuarioCliente":"13079","clienteEstrategico":"bxi","producto":"PORTAL DE INTERNET","anioVehiculo":str(anio),"categoria":cat,"marcaSubmarca":subMarca})
                            url_vehicles = 'https://www.segurosbanorte.com.mx/segurosbxi/vehiculos'
                            response_vehicles = requests.request("POST", self.url_vehicles, headers=self.headers, data=payload)
                            struct_vehicles = response_vehicles.json()

                            for struct_vehicle in struct_vehicles['data']['categorias']:
                                for vehicles in struct_vehicle['modelosVehiculo']:
                                    brand = vehicles['marca']
                                    subBrand = vehicles['submarca']
                                    claveBanorte = vehicles['claveBanorte']
                                    model  = str(vehicles['anio'])
                                    desc = vehicles['descripcion']

                                    values_catalog = {
                                        'categoria':cat,
                                        'id_categoria':id_cat,
                                        'marca' : brand,
                                        'id_marca' : id_subMarca,
                                        'submarca' : subBrand,
                                        'claveBanorte' : claveBanorte,
                                        'model': model,
                                        'descripcion' : desc,
                                        'fecha_extraccion' : date.today(),
                                    }
                                    self.df_catalog = self.df_catalog.append(values_catalog, ignore_index=True)
                                    print('Insertando: ' + str(self.df_catalog))
                except Exception as a:
                    print(a)

        try:
            engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
        except Exception as a:
                print('Error en la conexion banorte: ' + str(a))
        
        try:
            self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
        except Exception as a:
            try:
                self.df_catalog.to_csv(self.name_catalogo, sep='@', encoding='latin1')
            except Exception as e:
                print(e)
            pass 
        
    def run_spiders(self):
        '''
        Metodo run spiders
        Args: recibe el objeto para hacer funcionar el script get items
        desc: es el metodo encargado de hacer funcionar el script, este mismo manda 
        a ser llamado desde la clase principal llamada run_scrapers
        '''
        obj = self.get_items()
        print('TERMINO BANORTE')

