# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""
import requests
import configurations
import pandas as pd
from datetime import date
from sqlalchemy import create_engine

class Scraper:
    
    def __init__(self):
        '''
        Constructor 
            inicializa los atributos de los objetos que se usaran en el script
        ''' 

        self.df_catalog = pd.DataFrame()

        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database
        
        self.name_catalogo= 'catalogo_GNP.csv'
        self.name_table = 'gnp'
        
        self.tipo_auto_value = {
            'AUTOMOVIL':'AUT', 
            'PICK UP':'CA1',
            'MOTOCICLETA':'MOT'
            }

    def get_items(self, aut_val,aut):
        '''
        Metodo get items
        Args: Recibe los objetos inicializados  un diccionario con las llaves y los valores de los tipos de autos 
        Desc:  para este script se realizan peticiones concatenadas con algunos string para poder extraer el tipos de 
        vehiculos y en base a eso ir iterando para obtener marcas descripciones etc/
        '''
        
        s = requests.Session()
        contador = 0
        count = 0
        while (count < 3):
            try:
                list_models = s.get('https://gnp-vdi-pro.uc.r.appspot.com/_ah/api/gnp/v1/catalogo/modelos?tipovehiculo='+ aut_val).json()
                list_models = list(list_models['items'])
                length_requests = len(list_models)
                if(length_requests != 0):
                    break
            except Exception:
                count += 1
            
        for model in list_models:
            #Obtener marcas
            count = 0
            #print(model)
            while (count < 3):
                try:
                    list_brands = s.get('https://gnp-vdi-pro.uc.r.appspot.com/_ah/api/gnp/v1/catalogo/modelos/'+str(model)+'/marcas?tipovehiculo='+aut_val).json()
                    list_brands = list_brands['items']
                    length_requests = len(list_brands)
                    if(length_requests != 0):
                        break
                    else:
                        count += 1
                except Exception:
                    count += 1  
                
            for dic_brand in list_brands:
                brand = dic_brand['nombre']
                c_marca = dic_brand['clave']
                count = 0
                print(brand)
                while (count < 3):
                    try:
                        list_subbrands = s.get('https://gnp-vdi-pro.uc.r.appspot.com/_ah/api/gnp/v1/catalogo/marcas/'+str(c_marca)+'/modelos/'+str(model)+'/submarcas?tipovehiculo='+str(aut_val)).json()
                        list_subbrands = list_subbrands['items']
                        length_requests = len(list_subbrands)
                        if(length_requests != 0):
                            break
                        else:
                            count += 1
                    except Exception:
                        count += 1 
                        
                for dic_subbrand in list_subbrands:
                    submarca = dic_subbrand['nombre']
                    c_subbrand = dic_subbrand['clave']
                    count = 0
                    while (count < 3):
                        try:
                            list_description = s.get('https://gnp-vdi-pro.uc.r.appspot.com/_ah/api/gnp/v1/catalogo/marcas/'+str(c_marca)+'/modelos/'+str(model)+'/submarcas/'+str(c_subbrand)+'/versiones?tipovehiculo='+str(aut_val)).json()
                            list_description = list_description['items']
                            length_requests = len(list_description)
                            if(length_requests != 0):
                                break
                            else:
                                count += 1
                        except Exception:
                            count += 1 
                    for dic_description in list_description:
                        description = dic_description['nombre']
                        c_description = dic_description['clave']
                        contador += 1
                            
                        values_catalog = {
                            'Tipo':aut,
                            'C_tipo':aut_val,
                            'Modelo':model,
                            'Marca':brand,
                            'C_marca':c_marca,
                            'Submarca':submarca,
                            'C_submarca':c_subbrand,
                            'Descripcion':description,
                            'C_descripcion':c_description,
                            'fecha_extraccion':date.today(),
                        }
                        self.df_catalog = self.df_catalog.append(values_catalog, ignore_index=True)
                        print('Insertando: ' + str(self.df_catalog))

        try:
            engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
        except Exception as a:
            print('Error en la conexion gnp1: ' + str(a))
            
        try:
            self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
        except Exception as a:
            try:
                self.df_catalog.to_csv(self.name_catalogo, sep='@', encoding='latin1')
            except Exception as e:
                print(e)
            pass 


    #s.close()
    #return
    
    def run_spiders(self):
        '''
        Metodo run spiders
        Args: recibe el objetco para hacer funcionar el script get items
        desc: es el metodo encargado de hacer funcionar el script, este metodo itera los tipos de autos para 
        extraer los vehiculos. 
        '''
        for aut in self.tipo_auto_value.keys():
            print(self.tipo_auto_value[aut])
            print(aut)
            self.get_items(self.tipo_auto_value[aut],aut)

        print('termino GNP')
        