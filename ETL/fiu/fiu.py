# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

import requests
import pandas as pd
import configurations
from sqlalchemy import create_engine
from datetime import date

class Scraper:
    
    def __init__(self):
        '''
        Constructor 
            inicializa los atributos de los objetos que se usaran en el script
        '''

        self.df_catalog = pd.DataFrame()

        self.marcas_list = []
        self.id_list = []

        self.name_catalogo=('catalogo_Fiu.csv')
        self.name_table = ('fiu')
        
        self.url_models = 'https://fiu.mx/api/fiuUI/cotizacion/getMarcasPorModelo'
        self.url_marcas = 'https://fiu.mx/api/fiuUI/cotizacion/getLogosMarca'
        
        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database
        
    def get_items(self):
        '''
        Metodo get items
        Args: recibe los objetos inicializados
        Desc: Se realizan peticiones al portal y en cada una de ellas se va iterando las peticiones con 
        url_models y url_marcas crean estructuras asi en base a esas peticiones se obtienen los modelos 
        descripciones y submarcas. de este modo se guardan se almacenan en un dataframe el cual al termino
        es guardado en la db, en caso de cualquier exepcion sera guardado el catalogo en un Excel formato .csv
        '''
        
        struct_brands = requests.request("GET", self.url_marcas).json()
        struct_models = requests.request("GET", self.url_models).json()

        for array_marcas in struct_brands:
            self.marcas_list.append(array_marcas['marca'])
            self.id_list.append(array_marcas['idMarca'])
            
        for mod in struct_models:
            for id_mod in mod['marcas']:
                if(id_mod not in self.id_list):
                    mod['marcas'].remove(id_mod)
        
        for array_mod in struct_models:
            marcas = []
            for marc in array_mod['marcas']:
                marcas.append(self.marcas_list[self.id_list.index(marc)])
            array_mod['marca'] = marcas
            
        contador = 0
        limite = 3
        for array in struct_models:
            for marca in array['marca']:
                id_marca = str(array['marcas'][array['marca'].index(marca)])
                anio = str(array['modelo'])
                url_submarca = 'https://fiu.mx/api/fiuUI/cotizacion/getVehiculosPorModelo/' + id_marca + '/' + anio
                veces = 0
                while(veces < limite):
                    try:
                        struct_subbrand = requests.request("GET", url_submarca).json()
                        break
                    except Exception:
                        veces += 1
                for array_sub in struct_subbrand:
                    id_modelo = str(array_sub['idModeloVehiculo'])
                    modelo = str(array_sub['nombre'])
                    url_linea = 'https://fiu.mx/api/fiuUI/cotizacion/getLineasPorTipo/' + anio + '/' + id_marca + '/' + id_modelo
                    veces = 0
                    while(veces < limite):
                        try:
                            struct_line = requests.request("GET", url_linea).json()
                            break
                        except Exception:
                            veces += 1
                    for array_line in struct_line:
                        id_linea = str(array_line['idLinea'])
                        linea = str(array_line['linea'])
                        url_desc = 'https://fiu.mx/api/fiuUI/cotizacion/getDescripcionPorLinea/' + anio + '/' + id_marca + '/' + id_modelo + '/' + id_linea
                        veces = 0
                        while(veces < limite):
                            try:
                                struct_desc = requests.request("GET", url_desc).json()
                                break
                            except Exception:
                                veces += 1
                        for array_desc in struct_desc:
                            descripcion = str(array_desc['descripcion'])
                            clave = str(array_desc['claveSBI'])
                            id_desc = str(array_desc['idDescripcion'])
                                
                            values_catalog = {
                                'id_catalogo':contador,
                                'marca':marca,
                                'id_marca':id_marca,
                                'anio':anio,
                                'modelo':modelo,
                                'id_modelo':id_modelo,
                                'linea':linea,
                                'id_linea':id_linea,
                                'descripcion':descripcion,
                                'id_descripcion':id_desc,
                                'claveSBI':clave,
                                'fecha_extraccion':date.today(),
                                }

                        self.df_catalog = self.df_catalog.append(values_catalog, ignore_index=True)
                        print('Insertado: ' + str(self.df_catalog))
                        
        try:
            engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
        except Exception as a:
                print('Error en la conexion: ' + str(a))
        
        try:
            self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
        except Exception as a:
            try:
                self.df_catalog.to_csv(self.name_catalogo, sep='@', encoding='latin1')
            except Exception as e:
                print(e)
            pass 
          
    def run_spiders(self):
        '''
        Metodo run spiders
        Args: recibe el objeto para hacer funcionar el script get items
        desc: es el metodo encargado de hacer funcionar el script, este mismo manda 
        a ser llamado desde la clase principal llamada run_scrapers
        '''
        obj = self.get_items()
        print('total de registros extraidos FIU: ' + str(obj))
        