# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

import requests
import pandas as pd
import configurations
from selenium import webdriver
from sqlalchemy import create_engine
from datetime import date

class Scraper:
    
    def __init__(self):
        '''
        Constructor 
            inicializa los atributos de los objetos que se usaran en el script
        '''
        
        self.name_catalogo = ('catalogo_ahorra_seguro.csv')
        self.name_table = ('ahorraseguros')
        self.df_catalog = pd.DataFrame()

        self.url_principal = 'https://ahorraseguros.mx/seguros-de-autos/comparar-seguros-de-autos/#'
        self.xpath_find_elemnts = '/html/body/div[1]/div[2]/div/div[2]/div[2]/div/div/div/div[2]/div/div[2]/form/div[1]/div/div/div[2]'
        self.xpath_find_elemnts_marcs = '/html/body/div[1]/div[2]/div/div[2]/div[2]/div/div/div/div[2]/div/div[2]/form/div[1]/div/div/div[3]/ul/li/span/span'
        
        self.headers = {'authorization' : 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtZWpvcnNlZ3VybyIsImV4cCI6MTYzMjUwMTA5MywiaWF0IjoxNjMyNDE0NjkzfQ.mmOHjsLUSfIfiEM9oDD6htrat8UT1R8j3AjBojeDQzlZwJk7yAuukYgcrcBAXf9kXtYlKYuts7H0Z7Ba_13MYA'}
        
        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database
        
        
    def get_items(self):
        '''
        Metodo get items
        Args: recibe los objetos inicializados
        Desc: para este script usamos el driver geckoDriver de Mozila para usar la libreria selenium
        dado que tenemos que usar el evento .click() para asi realizar las peticiones, los datos se almacenan
        en un datadrame y al termino de este ser guardado en la bd, en cualquier exepcion sera guardado 
        en un Excel de formato .csv

        NOTA: cada extraccion se recomienda cambiar los headers (Authorization) para extraer datos 
        '''
        while(1):
            try:
                brands = []
                driver = webdriver.Firefox()
                driver.get(self.url_principal)
                driver.find_element_by_xpath(self.xpath_find_elemnts).click()
                request_marcs = driver.find_elements_by_xpath(self.xpath_find_elemnts_marcs)
                for brand in request_marcs:
                    brands.append(brand.text)
                driver.close()
                break
            except Exception as e:
                print(e)
        count = 0
        for brand in brands:
            url_models = 'https://core-ahorraseguros.com/v1/comparador/modelos?marca=' + brand
            request_count = 0
            while(request_count < 10):
                try:
                    models = requests.request("GET",url_models,headers = self.headers).json()
                    break
                except Exception as e:
                    print(e)
                    request_count += 1
            for mod in models:
                model = str(mod['text'])
                url_desc = 'https://core-ahorraseguros.com/v1/comparador/submarcas?marca=' + brand + '&modelo=' + model
                request_count = 0
                while(request_count < 10):
                    try:         
                        description = requests.request("GET",url_desc,headers = self.headers).json()
                        break
                    except Exception:
                        request_count += 1
                for desc in description:
                    description = str(desc['text'])
                    url_subdesc = 'https://core-ahorraseguros.com/v1/comparador/descripciones?marca=' + brand + '&modelo=' + model + '&submarca=' + description
                    request_count = 0
                    while(request_count < 10):
                        try:
                            sub_descs = requests.request("GET",url_subdesc,headers = self.headers).json()
                            break
                        except Exception:
                            request_count += 1
                    for sub in sub_descs:
                        sub_desc = str(sub['text'])

                        values_catalog = {
                            'Marca':brand,
                            'Modelo':model,
                            'Descripcion':description,
                            'sub_descripcion':sub_desc,
                            'fecha_extraccion': date.today(),
                            }
                        count += 1

                        self.df_catalog = self.df_catalog.append(values_catalog, ignore_index=True)  
                        print('Insertado: ' + str(self.df_catalog))
        

        try:
            engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))
        except Exception as a:
            print('Error en la conexion ahorraseguros: ' + str(a))
        
        try:
            self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
        except Exception as a:
            try:
                self.df_catalog.to_csv(self.name_catalogo , sep = '@', encoding = 'latin1')
            except Exception as a:
                print('Error al guardad en BD: ' + a)
            pass

        return count

    def run_spiders(self):
        '''
        Metodo run spiders
        Args: recibe el objeto para hacer funcionar el script get items
        desc: es el metodo encargado de hacer funcionar el script, este mismo manda 
        a ser llamado desde la clase principal llamada run_scrapers
        '''
        registros = self.get_items()
        print('total de registros insertados AHORRA SEGUROS ' + str(registros))
           
   
    
 