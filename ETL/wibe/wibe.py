# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""
import requests
import pandas as pd
import configurations
from sqlalchemy import create_engine
from datetime import date

class Scraper:

    def __init__(self):
        '''
        Constructor 
            inicializa los atributos de los objetos que se usaran en el script
        ''' 
        self.name_table = 'wibe'
        self.name_catalogo = 'catalogo_wibe.CSV'
        self.df_catalog = pd.DataFrame()
        
        self.headers = {
          'authority': 'www.wibe.com',
          'accept': 'application/json, text/plain, */*',
          'content-type': 'application/json;charset=UTF-8',
          'Cookie': 'JSESSIONID=0000Kb0rYOt_zU8RAJMPllmL17g:1eqm2rlc7; PD_STATEFUL_f9a2607a-405c-11e5-9d66-ac1064f2aa77=%2Fcotizadores; Cookie_Wibe_Cotizador=4184084652.41733.0000'
        }

        self.types_vh = {
                'auto':'[\"AUTOMOVILES\"]',
                'no_auto':'[\"PICKUP\",\"CAMIONES_HASTA_3_5_TONELADAS\"]'
                }
        
        self.url_models = "https://www.wibe.com/cotizadores/api/autos/aso/catalogos/modelos"
        self.url_version = 'https://www.wibe.com/cotizadores/api/autos/aso/catalogos/versiones'

        self.payload='{\"productoPlan\":{\"codigoProducto\":\"2008\",\"codigoPlan\":\"012\",\"revisionPlan\":\"001\",\"codigoRamo\":\"AUAR\"}}'

        self.host = configurations.host
        self.port = configurations.port
        self.user = configurations.user
        self.password = configurations.passwd
        self.DB = configurations.database

    def get_items(self):
        '''
        Metodo get items
        Args: recibe los objetos inicializados previemente por el constructor 
        Desc: El metodo realiza peticiones iniciando por los modelos para asi iterar en los tipos de vehiculos
        las peticiones solicitan payload armadas para extraer los datos complementarios, de este modo los 
        datos son guardados en un dataframe para que al termino sean guardados en la bd, dado el caso alguna 
        exepcion sera guardado en un Excel formato .csv
        '''
        response = requests.request("POST", self.url_models, headers=self.headers, data=self.payload)
        struct_models = response.json()
        contador = 0
        
        for dict_model in struct_models:
            model = str(dict_model['modelo'])
            
            for type_vh in self.types_vh.values():
                url_brands = 'https://www.wibe.com/cotizadores/api/autos/aso/catalogos/marcas'
                payload="{\"modelo\":\"" + model + "\",\"tipoVehiculo\":" + type_vh + ",\"listaTiposOrigenesVehiculos\":[\"NACIONAL\"],\"listaProductoPlan\":[{\"codigoProducto\":\"2008\",\"codigoPlan\":\"012\",\"revisionPlan\":\"001\",\"codigoRamo\":\"AUCR\"}]}"
                response = requests.request("POST", url_brands, headers=self.headers, data=payload)
                #print(response)
                struct_brands = response.json() 
                for struct_brand in struct_brands:
                    id_brand = str(struct_brand['codigoInterno'])
                    brand = struct_brand['descripcionInterna']
                    url_subbrand = 'https://www.wibe.com/cotizadores/api/autos/aso/catalogos/subMarcas'
                    payload="{\"modelo\":\"" + model + "\",\"tipoVehiculo\":" + type_vh + ",\"marcaVehiculo\":{\"codigoInterno\":\"" + id_brand + "\",\"descripcionInterna\":\"" + brand + "\",\"selected\":false},\"listaTiposOrigenVehiculo\":[\"NACIONAL\"],\"listaProductosPlan\":[{\"codigoProducto\":\"2008\",\"codigoPlan\":\"012\",\"revisionPlan\":\"001\",\"codigoRamo\":\"AUCR\"}]}"
                    response_subbrands = requests.request("POST", url_subbrand, headers=self.headers, data=payload)
                    struct_subbrands = response_subbrands.json()
                    for struct_subbrand in struct_subbrands:
                        id_subbrand = str(struct_subbrand['codigoInterno'])
                        subbrand = struct_subbrand['descripcionInterna']
                        
                        payload="{\"modelo\":\"" + model + "\",\"tipoVehiculo\":" + type_vh + ",\"marcaVehiculo\":{\"codigoInterno\":\"" + id_brand + "\",\"descripcionInterna\":\"" + brand + "\",\"selected\":false},\"listaProductosPlan\":[{\"codigoProducto\":\"2008\",\"codigoPlan\":\"012\",\"revisionPlan\":\"001\",\"codigoRamo\":\"AUAR\"}],\"listaTipoOrigenVehiculo\":[\"NACIONAL\"],\"subMarcaVehiculo\":{\"codigoInterno\":\"" + id_subbrand + "\",\"descripcionInterna\":\"" + subbrand + "\",\"selected\":false}}"
                        response_versiones = requests.request("POST", self.url_version, headers=self.headers, data=payload)
                        
                        try:
                              struct_versions = response_versiones.json()
                              for struct_version in struct_versions:
                                  id_version = struct_version['version']["codigoInterno"]
                                  version = struct_version['version']["descripcionInterna"]
                                  codigo_version = struct_version['codigoInternoAutomovil']
                                  
                                  values_catalog = {
                                      'tipo':type_vh.replace('"','').replace('[','').replace(']',''),
                                      'marca': brand,
                                      'id_marca': id_brand,
                                      'modelo': model,
                                      'submarca': subbrand,
                                      'id_subbrand': id_subbrand,
                                      'version': version,
                                      'id_version': id_version,
                                      'id_wibe' : codigo_version,
                                      'fecha_extraccion':date.today(),
                                      }
                                  
                                  self.df_catalog = self.df_catalog.append(values_catalog, ignore_index=True)
                                  print('Insertado: ' + str(self.df_catalog))
                        except Exception as e:
                              print(e)
        try:
            engine = create_engine("mysql+mysqlconnector://{user}:{pwd}@{host}:{port}/{db}".format(
                host=self.host, db=self.DB, user=self.user, pwd=self.password, port=self.port))                  
        except Exception as a:
            print('Error en conexion wibe:' + str(a))
        
        try:
            self.df_catalog.to_sql(self.name_table, con=engine, if_exists='append', index=False)
        except Exception as a:
            try:
                self.df_catalog.to_csv(self.name_catalogo , sep='@', encoding='latin1')
            except Exception as e:
                print(e)
            pass 
            
    def run_spiders(self):
        '''
        Metodo run spiders
        Args: recibe el objeto para hacer funcionar el script get items
        desc: es el metodo encargado de hacer funcionar el script, este mismo manda 
        a ser llamado desde la clase principal llamada run_scrapers
        '''
        registros = self.get_items()
        print('Total de Registros Extraidos Wibe: '+ str(registros))
