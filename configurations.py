# -*- coding: utf-8 -*-
"""
Created on Wed Jul  21 10:15:52 2021

@author: Efrain Martinez Cuate

@mail: efrain_twiins@hotmail.com

@cellphone: 55-76-09-59-330
"""

import configparser
from datetime import date

config = configparser.ConfigParser()
config.read('configurations.ini')

#fecha de extraccion
DATE = str(date.today())

#configuracion de la base de datos
host = config['DB']['host']
port = int(config['DB']['port'])
user = config['DB']['user']
passwd = config['DB']['passwd']
database = config['DB']['database']
auth_plugin = config['DB']['auth_plugin']

#lista de aseguradoras
list_players = config['players']['list_players'].split(',')

#lista de aseguradoras con peticion
request_list = config['players_request']['request_list'].split(',')

#lista de aseguradoras con scrappy
scrapyList = config['players_scrapy']['scrapyList'].split(',')

## ubmbrales para determinar el minimo de cotizaciones
thresholds = list(map(int, config['thresholds']['values'].split(',')))

##personaas a las que se le envia el mail de estatus de cotizacion
to_mail = config['mail_team']['mails'].split(',')

##cuenta desde donde se envia el mail
count_from_mail = config['count_from_mail']['from_user']
pass_from_mail = config['count_from_mail']['from_pass']
